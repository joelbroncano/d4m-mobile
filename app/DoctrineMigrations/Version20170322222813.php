<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170322222813 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE usuario_has_vehiculos (id INT AUTO_INCREMENT NOT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, activo_vehiculo VARCHAR(255) DEFAULT NULL, activo_conductor VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gcms (id INT AUTO_INCREMENT NOT NULL, click_action VARCHAR(255) DEFAULT NULL, sound VARCHAR(45) DEFAULT NULL, badge VARCHAR(45) DEFAULT NULL, icon VARCHAR(45) DEFAULT NULL, color VARCHAR(7) DEFAULT NULL, title VARCHAR(100) DEFAULT NULL, body TEXT DEFAULT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, estado TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE numero_telefonos (id INT AUTO_INCREMENT NOT NULL, numero VARCHAR(45) DEFAULT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, estado TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE metodo_pagos (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(100) DEFAULT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, estado TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE perfiles (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(45) NOT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, estado TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usuarios (id INT AUTO_INCREMENT NOT NULL, dni VARCHAR(8) DEFAULT NULL, username VARCHAR(45) DEFAULT NULL, nombre VARCHAR(100) DEFAULT NULL, apellido VARCHAR(150) DEFAULT NULL, email VARCHAR(150) DEFAULT NULL, password VARCHAR(150) DEFAULT NULL, salt VARCHAR(45) DEFAULT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, estado TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sessiones (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(100) DEFAULT NULL, token VARCHAR(150) DEFAULT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, estado TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE monedas (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(100) DEFAULT NULL, iso_code VARCHAR(3) NOT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, estado TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vehiculo_posiciones (id INT AUTO_INCREMENT NOT NULL, latitud DOUBLE PRECISION NOT NULL, longitud DOUBLE PRECISION NOT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, estado TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE clasificaciones (id INT AUTO_INCREMENT NOT NULL, tarifa VARCHAR(45) DEFAULT NULL, comentario TEXT DEFAULT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, estado TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dispositivos (id INT AUTO_INCREMENT NOT NULL, usuario_id INT DEFAULT NULL, imei VARCHAR(45) DEFAULT NULL, nombre VARCHAR(100) DEFAULT NULL, codigo_dispositivo VARCHAR(255) DEFAULT NULL, dispositivo_os VARCHAR(45) DEFAULT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, estado TINYINT(1) NOT NULL, INDEX fk_dispositivos_usuarios (usuario_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE taxi_servicio_solicitudes (id INT AUTO_INCREMENT NOT NULL, origen_latitud DOUBLE PRECISION DEFAULT NULL, origen_longitud DOUBLE PRECISION DEFAULT NULL, origen_direccion TEXT DEFAULT NULL, destino_latitud DOUBLE PRECISION DEFAULT NULL, destino_longitud DOUBLE PRECISION DEFAULT NULL, destino_direccion TEXT DEFAULT NULL, tiempo_solicitud TIME DEFAULT NULL, fecha_solicitud DATE DEFAULT NULL, numero_pasajeros INT DEFAULT NULL, numero_equipaje INT DEFAULT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, estado TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE permisos (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(100) DEFAULT NULL, grupo_permiso VARCHAR(45) DEFAULT NULL, grupo_permiso_tag VARCHAR(45) DEFAULT NULL, alias VARCHAR(45) DEFAULT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, estado TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vehiculos (id INT AUTO_INCREMENT NOT NULL, modelo VARCHAR(45) DEFAULT NULL, plato VARCHAR(8) DEFAULT NULL, color VARCHAR(45) NOT NULL, asientos_numericos INT DEFAULT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, estado TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE empresas (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(100) DEFAULT NULL, ruc VARCHAR(15) DEFAULT NULL, direccion TEXT DEFAULT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, estado TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE taxi_servicios (id INT AUTO_INCREMENT NOT NULL, created DATETIME NOT NULL, unique_id VARCHAR(45) DEFAULT NULL, created_where VARCHAR(255) DEFAULT NULL, updated DATETIME DEFAULT NULL, estado TINYINT(1) NOT NULL, estado_servicio TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pagos (id INT AUTO_INCREMENT NOT NULL, cantidad NUMERIC(15, 2) DEFAULT NULL, iso_code VARCHAR(3) NOT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, estado TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE dispositivos ADD CONSTRAINT FK_5C1C5F52DB38439E FOREIGN KEY (usuario_id) REFERENCES usuarios (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE dispositivos DROP FOREIGN KEY FK_5C1C5F52DB38439E');
        $this->addSql('DROP TABLE usuario_has_vehiculos');
        $this->addSql('DROP TABLE gcms');
        $this->addSql('DROP TABLE numero_telefonos');
        $this->addSql('DROP TABLE metodo_pagos');
        $this->addSql('DROP TABLE perfiles');
        $this->addSql('DROP TABLE usuarios');
        $this->addSql('DROP TABLE sessiones');
        $this->addSql('DROP TABLE monedas');
        $this->addSql('DROP TABLE vehiculo_posiciones');
        $this->addSql('DROP TABLE clasificaciones');
        $this->addSql('DROP TABLE dispositivos');
        $this->addSql('DROP TABLE taxi_servicio_solicitudes');
        $this->addSql('DROP TABLE permisos');
        $this->addSql('DROP TABLE vehiculos');
        $this->addSql('DROP TABLE empresas');
        $this->addSql('DROP TABLE taxi_servicios');
        $this->addSql('DROP TABLE pagos');
    }
}
