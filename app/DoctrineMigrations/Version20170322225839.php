<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170322225839 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE usuario_has_vehiculos ADD usuario_id INT NOT NULL, ADD vehiculo_id INT NOT NULL');
        $this->addSql('ALTER TABLE usuario_has_vehiculos ADD CONSTRAINT FK_55D8E836DB38439E FOREIGN KEY (usuario_id) REFERENCES usuarios (id)');
        $this->addSql('ALTER TABLE usuario_has_vehiculos ADD CONSTRAINT FK_55D8E83625F7D575 FOREIGN KEY (vehiculo_id) REFERENCES vehiculos (id)');
        $this->addSql('CREATE INDEX IDX_55D8E836DB38439E ON usuario_has_vehiculos (usuario_id)');
        $this->addSql('CREATE INDEX IDX_55D8E83625F7D575 ON usuario_has_vehiculos (vehiculo_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE usuario_has_vehiculos DROP FOREIGN KEY FK_55D8E836DB38439E');
        $this->addSql('ALTER TABLE usuario_has_vehiculos DROP FOREIGN KEY FK_55D8E83625F7D575');
        $this->addSql('DROP INDEX IDX_55D8E836DB38439E ON usuario_has_vehiculos');
        $this->addSql('DROP INDEX IDX_55D8E83625F7D575 ON usuario_has_vehiculos');
        $this->addSql('ALTER TABLE usuario_has_vehiculos DROP usuario_id, DROP vehiculo_id');
    }
}
