<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170323204432 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE usuarios ADD usuario_id_refer INT NOT NULL');
        $this->addSql('ALTER TABLE usuarios ADD CONSTRAINT FK_EF687F2A557C84A FOREIGN KEY (usuario_id_refer) REFERENCES usuarios (id)');
        $this->addSql('CREATE INDEX IDX_EF687F2A557C84A ON usuarios (usuario_id_refer)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE usuarios DROP FOREIGN KEY FK_EF687F2A557C84A');
        $this->addSql('DROP INDEX IDX_EF687F2A557C84A ON usuarios');
        $this->addSql('ALTER TABLE usuarios DROP usuario_id_refer');
    }
}
