<?php

namespace MainBundle\Entity;

/**
 * Dispositivo
 */
class Dispositivo
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $imei;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $codigoDispositivo;

    /**
     * @var string
     */
    private $dispositivoOs;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var boolean
     */
    private $estado;

    /**
     * @var \MainBundle\Entity\Usuario
     */
    private $usuario;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imei
     *
     * @param string $imei
     *
     * @return Dispositivo
     */
    public function setImei($imei)
    {
        $this->imei = $imei;

        return $this;
    }

    /**
     * Get imei
     *
     * @return string
     */
    public function getImei()
    {
        return $this->imei;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Dispositivo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set codigoDispositivo
     *
     * @param string $codigoDispositivo
     *
     * @return Dispositivo
     */
    public function setCodigoDispositivo($codigoDispositivo)
    {
        $this->codigoDispositivo = $codigoDispositivo;

        return $this;
    }

    /**
     * Get codigoDispositivo
     *
     * @return string
     */
    public function getCodigoDispositivo()
    {
        return $this->codigoDispositivo;
    }

    /**
     * Set dispositivoOs
     *
     * @param string $dispositivoOs
     *
     * @return Dispositivo
     */
    public function setDispositivoOs($dispositivoOs)
    {
        $this->dispositivoOs = $dispositivoOs;

        return $this;
    }

    /**
     * Get dispositivoOs
     *
     * @return string
     */
    public function getDispositivoOs()
    {
        return $this->dispositivoOs;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Dispositivo
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Dispositivo
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     *
     * @return Dispositivo
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set usuario
     *
     * @param \MainBundle\Entity\Usuario $usuario
     *
     * @return Dispositivo
     */
    public function setUsuario(\MainBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \MainBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}
