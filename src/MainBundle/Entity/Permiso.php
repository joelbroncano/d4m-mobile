<?php

namespace MainBundle\Entity;

/**
 * Permiso
 */
class Permiso
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $grupoPermiso;

    /**
     * @var string
     */
    private $grupoPermisoTag;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var boolean
     */
    private $estado;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Permiso
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set grupoPermiso
     *
     * @param string $grupoPermiso
     *
     * @return Permiso
     */
    public function setGrupoPermiso($grupoPermiso)
    {
        $this->grupoPermiso = $grupoPermiso;

        return $this;
    }

    /**
     * Get grupoPermiso
     *
     * @return string
     */
    public function getGrupoPermiso()
    {
        return $this->grupoPermiso;
    }

    /**
     * Set grupoPermisoTag
     *
     * @param string $grupoPermisoTag
     *
     * @return Permiso
     */
    public function setGrupoPermisoTag($grupoPermisoTag)
    {
        $this->grupoPermisoTag = $grupoPermisoTag;

        return $this;
    }

    /**
     * Get grupoPermisoTag
     *
     * @return string
     */
    public function getGrupoPermisoTag()
    {
        return $this->grupoPermisoTag;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return Permiso
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Permiso
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Permiso
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     *
     * @return Permiso
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean
     */
    public function getEstado()
    {
        return $this->estado;
    }
}
