<?php

namespace MainBundle\Entity;

/**
 * TaxiServicio
 */
class TaxiServicio
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var string
     */
    private $uniqueId;

    /**
     * @var string
     */
    private $createdWhere;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var boolean
     */
    private $estado;

    /**
     * @var boolean
     */
    private $estadoServicio;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return TaxiServicio
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set uniqueId
     *
     * @param string $uniqueId
     *
     * @return TaxiServicio
     */
    public function setUniqueId($uniqueId)
    {
        $this->uniqueId = $uniqueId;

        return $this;
    }

    /**
     * Get uniqueId
     *
     * @return string
     */
    public function getUniqueId()
    {
        return $this->uniqueId;
    }

    /**
     * Set createdWhere
     *
     * @param string $createdWhere
     *
     * @return TaxiServicio
     */
    public function setCreatedWhere($createdWhere)
    {
        $this->createdWhere = $createdWhere;

        return $this;
    }

    /**
     * Get createdWhere
     *
     * @return string
     */
    public function getCreatedWhere()
    {
        return $this->createdWhere;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return TaxiServicio
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     *
     * @return TaxiServicio
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set estadoServicio
     *
     * @param boolean $estadoServicio
     *
     * @return TaxiServicio
     */
    public function setEstadoServicio($estadoServicio)
    {
        $this->estadoServicio = $estadoServicio;

        return $this;
    }

    /**
     * Get estadoServicio
     *
     * @return boolean
     */
    public function getEstadoServicio()
    {
        return $this->estadoServicio;
    }
}
