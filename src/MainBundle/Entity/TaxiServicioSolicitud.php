<?php

namespace MainBundle\Entity;

/**
 * TaxiServicioSolicitud
 */
class TaxiServicioSolicitud
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var float
     */
    private $origenLatitud;

    /**
     * @var float
     */
    private $origenLongitud;

    /**
     * @var string
     */
    private $origenDireccion;

    /**
     * @var float
     */
    private $destinoLatitud;

    /**
     * @var float
     */
    private $destinoLongitud;

    /**
     * @var string
     */
    private $destinoDireccion;

    /**
     * @var \DateTime
     */
    private $tiempoSolicitud;

    /**
     * @var \DateTime
     */
    private $FechaSolicitud;

    /**
     * @var integer
     */
    private $numeroPasajeros;

    /**
     * @var integer
     */
    private $numeroEquipaje;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var boolean
     */
    private $estado;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set origenLatitud
     *
     * @param float $origenLatitud
     *
     * @return TaxiServicioSolicitud
     */
    public function setOrigenLatitud($origenLatitud)
    {
        $this->origenLatitud = $origenLatitud;

        return $this;
    }

    /**
     * Get origenLatitud
     *
     * @return float
     */
    public function getOrigenLatitud()
    {
        return $this->origenLatitud;
    }

    /**
     * Set origenLongitud
     *
     * @param float $origenLongitud
     *
     * @return TaxiServicioSolicitud
     */
    public function setOrigenLongitud($origenLongitud)
    {
        $this->origenLongitud = $origenLongitud;

        return $this;
    }

    /**
     * Get origenLongitud
     *
     * @return float
     */
    public function getOrigenLongitud()
    {
        return $this->origenLongitud;
    }

    /**
     * Set origenDireccion
     *
     * @param string $origenDireccion
     *
     * @return TaxiServicioSolicitud
     */
    public function setOrigenDireccion($origenDireccion)
    {
        $this->origenDireccion = $origenDireccion;

        return $this;
    }

    /**
     * Get origenDireccion
     *
     * @return string
     */
    public function getOrigenDireccion()
    {
        return $this->origenDireccion;
    }

    /**
     * Set destinoLatitud
     *
     * @param float $destinoLatitud
     *
     * @return TaxiServicioSolicitud
     */
    public function setDestinoLatitud($destinoLatitud)
    {
        $this->destinoLatitud = $destinoLatitud;

        return $this;
    }

    /**
     * Get destinoLatitud
     *
     * @return float
     */
    public function getDestinoLatitud()
    {
        return $this->destinoLatitud;
    }

    /**
     * Set destinoLongitud
     *
     * @param float $destinoLongitud
     *
     * @return TaxiServicioSolicitud
     */
    public function setDestinoLongitud($destinoLongitud)
    {
        $this->destinoLongitud = $destinoLongitud;

        return $this;
    }

    /**
     * Get destinoLongitud
     *
     * @return float
     */
    public function getDestinoLongitud()
    {
        return $this->destinoLongitud;
    }

    /**
     * Set destinoDireccion
     *
     * @param string $destinoDireccion
     *
     * @return TaxiServicioSolicitud
     */
    public function setDestinoDireccion($destinoDireccion)
    {
        $this->destinoDireccion = $destinoDireccion;

        return $this;
    }

    /**
     * Get destinoDireccion
     *
     * @return string
     */
    public function getDestinoDireccion()
    {
        return $this->destinoDireccion;
    }

    /**
     * Set tiempoSolicitud
     *
     * @param \DateTime $tiempoSolicitud
     *
     * @return TaxiServicioSolicitud
     */
    public function setTiempoSolicitud($tiempoSolicitud)
    {
        $this->tiempoSolicitud = $tiempoSolicitud;

        return $this;
    }

    /**
     * Get tiempoSolicitud
     *
     * @return \DateTime
     */
    public function getTiempoSolicitud()
    {
        return $this->tiempoSolicitud;
    }

    /**
     * Set fechaSolicitud
     *
     * @param \DateTime $fechaSolicitud
     *
     * @return TaxiServicioSolicitud
     */
    public function setFechaSolicitud($fechaSolicitud)
    {
        $this->FechaSolicitud = $fechaSolicitud;

        return $this;
    }

    /**
     * Get fechaSolicitud
     *
     * @return \DateTime
     */
    public function getFechaSolicitud()
    {
        return $this->FechaSolicitud;
    }

    /**
     * Set numeroPasajeros
     *
     * @param integer $numeroPasajeros
     *
     * @return TaxiServicioSolicitud
     */
    public function setNumeroPasajeros($numeroPasajeros)
    {
        $this->numeroPasajeros = $numeroPasajeros;

        return $this;
    }

    /**
     * Get numeroPasajeros
     *
     * @return integer
     */
    public function getNumeroPasajeros()
    {
        return $this->numeroPasajeros;
    }

    /**
     * Set numeroEquipaje
     *
     * @param integer $numeroEquipaje
     *
     * @return TaxiServicioSolicitud
     */
    public function setNumeroEquipaje($numeroEquipaje)
    {
        $this->numeroEquipaje = $numeroEquipaje;

        return $this;
    }

    /**
     * Get numeroEquipaje
     *
     * @return integer
     */
    public function getNumeroEquipaje()
    {
        return $this->numeroEquipaje;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return TaxiServicioSolicitud
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return TaxiServicioSolicitud
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     *
     * @return TaxiServicioSolicitud
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean
     */
    public function getEstado()
    {
        return $this->estado;
    }
}
