<?php

namespace MainBundle\Entity;

/**
 * UsuarioHasVehiculo
 */
class UsuarioHasVehiculo
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var string
     */
    private $activoVehiculo;

    /**
     * @var string
     */
    private $activoConductor;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return UsuarioHasVehiculo
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return UsuarioHasVehiculo
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set activoVehiculo
     *
     * @param string $activoVehiculo
     *
     * @return UsuarioHasVehiculo
     */
    public function setActivoVehiculo($activoVehiculo)
    {
        $this->activoVehiculo = $activoVehiculo;

        return $this;
    }

    /**
     * Get activoVehiculo
     *
     * @return string
     */
    public function getActivoVehiculo()
    {
        return $this->activoVehiculo;
    }

    /**
     * Set activoConductor
     *
     * @param string $activoConductor
     *
     * @return UsuarioHasVehiculo
     */
    public function setActivoConductor($activoConductor)
    {
        $this->activoConductor = $activoConductor;

        return $this;
    }

    /**
     * Get activoConductor
     *
     * @return string
     */
    public function getActivoConductor()
    {
        return $this->activoConductor;
    }
    /**
     * @var \MainBundle\Entity\Usuario
     */
    private $usuario;

    /**
     * @var \MainBundle\Entity\Vehiculo
     */
    private $vehiculo;


    /**
     * Set usuario
     *
     * @param \MainBundle\Entity\Usuario $usuario
     *
     * @return UsuarioHasVehiculo
     */
    public function setUsuario(\MainBundle\Entity\Usuario $usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \MainBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set vehiculo
     *
     * @param \MainBundle\Entity\Vehiculo $vehiculo
     *
     * @return UsuarioHasVehiculo
     */
    public function setVehiculo(\MainBundle\Entity\Vehiculo $vehiculo)
    {
        $this->vehiculo = $vehiculo;

        return $this;
    }

    /**
     * Get vehiculo
     *
     * @return \MainBundle\Entity\Vehiculo
     */
    public function getVehiculo()
    {
        return $this->vehiculo;
    }
}
