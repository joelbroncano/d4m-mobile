<?php

namespace MainBundle\Entity;

/**
 * Vehiculo
 */
class Vehiculo
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $modelo;

    /**
     * @var string
     */
    private $plato;

    /**
     * @var string
     */
    private $color;

    /**
     * @var integer
     */
    private $asientosNumericos;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var boolean
     */
    private $estado;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set modelo
     *
     * @param string $modelo
     *
     * @return Vehiculo
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo
     *
     * @return string
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set plato
     *
     * @param string $plato
     *
     * @return Vehiculo
     */
    public function setPlato($plato)
    {
        $this->plato = $plato;

        return $this;
    }

    /**
     * Get plato
     *
     * @return string
     */
    public function getPlato()
    {
        return $this->plato;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Vehiculo
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set asientosNumericos
     *
     * @param integer $asientosNumericos
     *
     * @return Vehiculo
     */
    public function setAsientosNumericos($asientosNumericos)
    {
        $this->asientosNumericos = $asientosNumericos;

        return $this;
    }

    /**
     * Get asientosNumericos
     *
     * @return integer
     */
    public function getAsientosNumericos()
    {
        return $this->asientosNumericos;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Vehiculo
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Vehiculo
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     *
     * @return Vehiculo
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean
     */
    public function getEstado()
    {
        return $this->estado;
    }
}
