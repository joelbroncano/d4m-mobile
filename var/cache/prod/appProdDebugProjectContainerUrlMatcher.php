<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdDebugProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/main')) {
            // main_homepage
            if (rtrim($pathinfo, '/') === '/main') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'main_homepage');
                }

                return array (  '_controller' => 'MainBundle\\Controller\\DefaultController::indexAction',  '_route' => 'main_homepage',);
            }

            if (0 === strpos($pathinfo, '/main/security/log')) {
                if (0 === strpos($pathinfo, '/main/security/login')) {
                    // main_security_login
                    if ($pathinfo === '/main/security/login') {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_main_security_login;
                        }

                        return array (  '_controller' => 'MainBundle\\Controller\\SecurityController::loginAction',  '_route' => 'main_security_login',);
                    }
                    not_main_security_login:

                    // main_security_login_check
                    if ($pathinfo === '/main/security/login_check') {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_main_security_login_check;
                        }

                        return array (  '_controller' => 'MainBundle\\Controller\\SecurityController::loginCheckAction',  '_route' => 'main_security_login_check',);
                    }
                    not_main_security_login_check:

                }

                // main_security_logout
                if ($pathinfo === '/main/security/logout') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_main_security_logout;
                    }

                    return array (  '_controller' => 'MainBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'main_security_logout',);
                }
                not_main_security_logout:

            }

        }

        if (0 === strpos($pathinfo, '/ap')) {
            // api_homepage
            if (rtrim($pathinfo, '/') === '/api') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'api_homepage');
                }

                return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::indexAction',  '_route' => 'api_homepage',);
            }

            // homepage
            if (rtrim($pathinfo, '/') === '/app') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'homepage');
                }

                return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
