<?php

/* MainBundle:Security:login.html.twig */
class __TwigTemplate_8d015b45f20194e4fc3f8bb4e4d4e0e97c66e8a1435f3121bfea48e920063f56 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MainBundle::template.html.twig", "MainBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MainBundle::template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c1799d6054298425e813d523ab6923a27a8b3f8e395efe158248d0f7f04bb08b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c1799d6054298425e813d523ab6923a27a8b3f8e395efe158248d0f7f04bb08b->enter($__internal_c1799d6054298425e813d523ab6923a27a8b3f8e395efe158248d0f7f04bb08b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MainBundle:Security:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c1799d6054298425e813d523ab6923a27a8b3f8e395efe158248d0f7f04bb08b->leave($__internal_c1799d6054298425e813d523ab6923a27a8b3f8e395efe158248d0f7f04bb08b_prof);

    }

    // line 3
    public function block_contenido($context, array $blocks = array())
    {
        $__internal_a4f093ee5b7f6a3fff63f297786bb52566791c41b0a8165478518ec069847d9a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a4f093ee5b7f6a3fff63f297786bb52566791c41b0a8165478518ec069847d9a->enter($__internal_a4f093ee5b7f6a3fff63f297786bb52566791c41b0a8165478518ec069847d9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        // line 4
        echo "
    <div class=\"login-box\">
        <div class=\"login-logo\">
            <a href=\"../../index2.html\"><b>Taxi</b>D4M</a>
        </div><!-- /.login-logo -->
        <div class=\"login-box-body\">
            <p class=\"login-box-msg\">Inicia sesión</p>

            ";
        // line 12
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 13
            echo "                <div class=\"info-box bg-red\">
                    <span class=\"info-box-icon\"><i class=\"fa fa-fw fa-close\"></i></span>
                    <div class=\"info-box-content\">
                        <span class=\"info-box-text\">Error</span>
                        <span class=\"info-box-number\">";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</span>
                        <div class=\"progress\">
                            <div class=\"progress-bar\" style=\"width: 70%\"></div>
                        </div>
                  <span class=\"progress-description\">
                    -
                  </span>
                    </div><!-- /.info-box-content -->
                </div>
            ";
        }
        // line 27
        echo "
            <form action=\"";
        // line 28
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("main_security_login_check");
        echo "\" method=\"post\">
                <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderCsrfToken("authenticate"), "html", null, true);
        echo "\">

                <div class=\"form-group has-feedback\">
                    <input type=\"text\" class=\"form-control\" id=\"username\" name=\"_username\" value=\"";
        // line 32
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" placeholder=\"Username\"/>
                    <span class=\"glyphicon glyphicon-user form-control-feedback\"></span>
                </div>
                <div class=\"form-group has-feedback\">
                    <input type=\"password\" class=\"form-control\" id=\"password\" name=\"_password\" placeholder=\"Password\">
                    <span class=\"glyphicon glyphicon-lock form-control-feedback\"></span>
                </div>
                <div class=\"row\">
                    <div class=\"col-xs-8\">
                        <div class=\"checkbox icheck\">
                            <label>
                                <input type=\"checkbox\"> Recordar
                            </label>
                        </div>
                    </div><!-- /.col -->
                    <div class=\"col-xs-4\">
                        <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Log in</button>
                    </div><!-- /.col -->
                </div>
            </form>

            ";
        // line 54
        echo "            ";
        // line 55
        echo "            ";
        // line 56
        echo "            ";
        // line 57
        echo "            ";
        // line 58
        echo "
            ";
        // line 60
        echo "            ";
        // line 61
        echo "
        </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->








";
        
        $__internal_a4f093ee5b7f6a3fff63f297786bb52566791c41b0a8165478518ec069847d9a->leave($__internal_a4f093ee5b7f6a3fff63f297786bb52566791c41b0a8165478518ec069847d9a_prof);

    }

    public function getTemplateName()
    {
        return "MainBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 61,  119 => 60,  116 => 58,  114 => 57,  112 => 56,  110 => 55,  108 => 54,  84 => 32,  78 => 29,  74 => 28,  71 => 27,  58 => 17,  52 => 13,  50 => 12,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"MainBundle::template.html.twig\" %}

{% block contenido %}

    <div class=\"login-box\">
        <div class=\"login-logo\">
            <a href=\"../../index2.html\"><b>Taxi</b>D4M</a>
        </div><!-- /.login-logo -->
        <div class=\"login-box-body\">
            <p class=\"login-box-msg\">Inicia sesión</p>

            {% if error %}
                <div class=\"info-box bg-red\">
                    <span class=\"info-box-icon\"><i class=\"fa fa-fw fa-close\"></i></span>
                    <div class=\"info-box-content\">
                        <span class=\"info-box-text\">Error</span>
                        <span class=\"info-box-number\">{{ error.messageKey|trans(error.messageData, 'security') }}</span>
                        <div class=\"progress\">
                            <div class=\"progress-bar\" style=\"width: 70%\"></div>
                        </div>
                  <span class=\"progress-description\">
                    -
                  </span>
                    </div><!-- /.info-box-content -->
                </div>
            {% endif %}

            <form action=\"{{ path('main_security_login_check') }}\" method=\"post\">
                <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token('authenticate') }}\">

                <div class=\"form-group has-feedback\">
                    <input type=\"text\" class=\"form-control\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" placeholder=\"Username\"/>
                    <span class=\"glyphicon glyphicon-user form-control-feedback\"></span>
                </div>
                <div class=\"form-group has-feedback\">
                    <input type=\"password\" class=\"form-control\" id=\"password\" name=\"_password\" placeholder=\"Password\">
                    <span class=\"glyphicon glyphicon-lock form-control-feedback\"></span>
                </div>
                <div class=\"row\">
                    <div class=\"col-xs-8\">
                        <div class=\"checkbox icheck\">
                            <label>
                                <input type=\"checkbox\"> Recordar
                            </label>
                        </div>
                    </div><!-- /.col -->
                    <div class=\"col-xs-4\">
                        <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Log in</button>
                    </div><!-- /.col -->
                </div>
            </form>

            {#<div class=\"social-auth-links text-center\">#}
            {#<p>- OR -</p>#}
            {#<a href=\"#\" class=\"btn btn-block btn-social btn-facebook btn-flat\"><i class=\"fa fa-facebook\"></i> Sign in using Facebook</a>#}
            {#<a href=\"#\" class=\"btn btn-block btn-social btn-google btn-flat\"><i class=\"fa fa-google-plus\"></i> Sign in using Google+</a>#}
            {#</div><!-- /.social-auth-links -->#}

            {#<a href=\"#\">I forgot my password</a><br>#}
            {#<a href=\"register.html\" class=\"text-center\">Register a new membership</a>#}

        </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->








{% endblock %}


{#{% extends \"MainBundle::template.html.twig\" %}#}
{#{% block contenido %}#}
    {#{% if error %}#}
        {#<div style=\"color:white\">{{ error.message }}</div>#}
    {#{% endif %}#}

    {#<div class=\"login-panel panel panel-default\">#}
        {#<div class=\"panel-heading\">#}
            {#Web de Consulta#}
        {#</div>#}
        {#<div class=\"panel-body\">#}
            {#<form action=\"{{ path(\"overall_ssri_consultaB_security_login_check\") }}\" method=\"post\" role=\"form\">#}
                {#<fieldset>#}
                    {#<input type=\"hidden\" name=\"_csrf_token\"#}
                           {#value=\"{{ csrf_token('authenticate') }}\"#}
                            {#>#}
                    {#<div class=\"form-group\">#}
                        {#&#123;&#35;                    <label for=\"username\">{{ 'security.login.username'|trans }}</label>&#35;&#125;#}
                        {#<input type=\"text\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" required=\"required\" class=\"form-control\"#}
                               {#placeholder=\"Usuario\" autofocus/>#}
                    {#</div>#}
                    {#<div class=\"form-group\">#}
                        {#&#123;&#35;<label for=\"password\">{{ 'security.login.password'|trans }}</label>&#35;&#125;#}
                        {#<input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" class=\"form-control\" placeholder=\"Contraseña\"#}
                               {#class=\"form-control\"/>#}
                    {#</div>#}
                    {#<div class=\"checkbox\">#}
                        {#<label>#}
                            {#<input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\"/>Recordar sesión#}
                        {#</label>#}
                    {#</div>#}
                    {#<input type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"Ingresar\" class=\"btn btn-lg btn-success btn-block\"/>#}
                {#</fieldset>#}
            {#</form>#}
        {#</div>#}
    {#</div>#}
{#{% endblock %}#}












", "MainBundle:Security:login.html.twig", "/var/www/html/taxi-backend/src/MainBundle/Resources/views/Security/login.html.twig");
    }
}
