<?php

/* base.html.twig */
class __TwigTemplate_fdeeabc134662d984022a86e9dccc4e1da57de69e2c4de15364ae341e6da1136 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_009640adad3ec2c5525130325c733f18552d1f7434e81ee0576c2223a5807ff8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_009640adad3ec2c5525130325c733f18552d1f7434e81ee0576c2223a5807ff8->enter($__internal_009640adad3ec2c5525130325c733f18552d1f7434e81ee0576c2223a5807ff8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_009640adad3ec2c5525130325c733f18552d1f7434e81ee0576c2223a5807ff8->leave($__internal_009640adad3ec2c5525130325c733f18552d1f7434e81ee0576c2223a5807ff8_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_789454bb74cdfa3c2c11cb4be24a234251b3c13ac254193cac32549b2f52d409 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_789454bb74cdfa3c2c11cb4be24a234251b3c13ac254193cac32549b2f52d409->enter($__internal_789454bb74cdfa3c2c11cb4be24a234251b3c13ac254193cac32549b2f52d409_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_789454bb74cdfa3c2c11cb4be24a234251b3c13ac254193cac32549b2f52d409->leave($__internal_789454bb74cdfa3c2c11cb4be24a234251b3c13ac254193cac32549b2f52d409_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_1665a555ddd6e349383dba6fd9b22217d95a49349311e74fec1b39214057a656 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1665a555ddd6e349383dba6fd9b22217d95a49349311e74fec1b39214057a656->enter($__internal_1665a555ddd6e349383dba6fd9b22217d95a49349311e74fec1b39214057a656_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_1665a555ddd6e349383dba6fd9b22217d95a49349311e74fec1b39214057a656->leave($__internal_1665a555ddd6e349383dba6fd9b22217d95a49349311e74fec1b39214057a656_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_20b0d65cbbfd1130d27e710aa6594dc7026788afcc64b041139d4c8145606f70 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_20b0d65cbbfd1130d27e710aa6594dc7026788afcc64b041139d4c8145606f70->enter($__internal_20b0d65cbbfd1130d27e710aa6594dc7026788afcc64b041139d4c8145606f70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_20b0d65cbbfd1130d27e710aa6594dc7026788afcc64b041139d4c8145606f70->leave($__internal_20b0d65cbbfd1130d27e710aa6594dc7026788afcc64b041139d4c8145606f70_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_f1723270655c46019490d658261f4469183d0f8868a2748a546316dd514568d8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f1723270655c46019490d658261f4469183d0f8868a2748a546316dd514568d8->enter($__internal_f1723270655c46019490d658261f4469183d0f8868a2748a546316dd514568d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_f1723270655c46019490d658261f4469183d0f8868a2748a546316dd514568d8->leave($__internal_f1723270655c46019490d658261f4469183d0f8868a2748a546316dd514568d8_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/var/www/html/taxi-backend/app/Resources/views/base.html.twig");
    }
}
