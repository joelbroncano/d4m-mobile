<?php

/* MainBundle::template.html.twig */
class __TwigTemplate_8e3227655bdb52cf6dd05083c2d139a9fefe36238252f7cebc18a26ee845504c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'contenido' => array($this, 'block_contenido'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0acdab7830614c0772ae8ea5d02dea92a1ef09337c2b5a4e8625b20945d48692 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0acdab7830614c0772ae8ea5d02dea92a1ef09337c2b5a4e8625b20945d48692->enter($__internal_0acdab7830614c0772ae8ea5d02dea92a1ef09337c2b5a4e8625b20945d48692_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MainBundle::template.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <title>Taxi-d4m | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">

    ";
        // line 10
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 26
        echo "
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src=\"{ { asset('bundles/main/html5shiv/html5shiv.min.js')}}\"></script>-->
    <!--<script src=\"{ { asset('bundles/main/respond/respond.min.js')}}\"></script>-->
    <![endif]-->
</head>
<body class=\"hold-transition login-page\">

";
        // line 36
        $this->displayBlock('contenido', $context, $blocks);
        // line 39
        echo "
";
        // line 40
        $this->displayBlock('javascripts', $context, $blocks);
        // line 60
        echo "
</body>
</html>
";
        
        $__internal_0acdab7830614c0772ae8ea5d02dea92a1ef09337c2b5a4e8625b20945d48692->leave($__internal_0acdab7830614c0772ae8ea5d02dea92a1ef09337c2b5a4e8625b20945d48692_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_60fbc6946a94851751825d15144a750db6894c13050482f3d2d8e5ded24d45ab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_60fbc6946a94851751825d15144a750db6894c13050482f3d2d8e5ded24d45ab->enter($__internal_60fbc6946a94851751825d15144a750db6894c13050482f3d2d8e5ded24d45ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "        <!-- Bootstrap 3.3.5 -->
        <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/main/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\">

        <!-- Font Awesome -->
        <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/main/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\">

        <!-- Ionicons -->
        <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/main/ionicons/ionicons.min.css"), "html", null, true);
        echo "\">

        <!-- Theme style -->
        <link rel=\"stylesheet\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/main/dist/css/AdminLTE.min.css"), "html", null, true);
        echo "\">

        <!-- iCheck -->
        <link rel=\"stylesheet\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/main/plugins/iCheck/square/blue.css"), "html", null, true);
        echo "\">
    ";
        
        $__internal_60fbc6946a94851751825d15144a750db6894c13050482f3d2d8e5ded24d45ab->leave($__internal_60fbc6946a94851751825d15144a750db6894c13050482f3d2d8e5ded24d45ab_prof);

    }

    // line 36
    public function block_contenido($context, array $blocks = array())
    {
        $__internal_cb57489be00e4b496fae9b0f18c279d883aab9206f1a5f483c23fa4d1813835d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb57489be00e4b496fae9b0f18c279d883aab9206f1a5f483c23fa4d1813835d->enter($__internal_cb57489be00e4b496fae9b0f18c279d883aab9206f1a5f483c23fa4d1813835d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenido"));

        // line 37
        echo "
";
        
        $__internal_cb57489be00e4b496fae9b0f18c279d883aab9206f1a5f483c23fa4d1813835d->leave($__internal_cb57489be00e4b496fae9b0f18c279d883aab9206f1a5f483c23fa4d1813835d_prof);

    }

    // line 40
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_5e11844bb682222597e3a1182b36c4e6367ef1c59f12107c78f9d7908f7b6da5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5e11844bb682222597e3a1182b36c4e6367ef1c59f12107c78f9d7908f7b6da5->enter($__internal_5e11844bb682222597e3a1182b36c4e6367ef1c59f12107c78f9d7908f7b6da5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 41
        echo "    <!-- jQuery 2.1.4 -->
    <script src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/main/plugins/jQuery/jQuery-2.1.4.min.js"), "html", null, true);
        echo "\"></script>

    <!-- Bootstrap 3.3.5 -->
    <script src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/main/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

    <!-- iCheck -->
    <script src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/main/plugins/iCheck/icheck.min.js"), "html", null, true);
        echo "\"></script>

    <script>
        \$(function () {
            \$('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
";
        
        $__internal_5e11844bb682222597e3a1182b36c4e6367ef1c59f12107c78f9d7908f7b6da5->leave($__internal_5e11844bb682222597e3a1182b36c4e6367ef1c59f12107c78f9d7908f7b6da5_prof);

    }

    public function getTemplateName()
    {
        return "MainBundle::template.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  144 => 48,  138 => 45,  132 => 42,  129 => 41,  123 => 40,  115 => 37,  109 => 36,  100 => 24,  94 => 21,  88 => 18,  82 => 15,  76 => 12,  73 => 11,  67 => 10,  57 => 60,  55 => 40,  52 => 39,  50 => 36,  38 => 26,  36 => 10,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <title>Taxi-d4m | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">

    {% block stylesheets %}
        <!-- Bootstrap 3.3.5 -->
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/main/bootstrap/css/bootstrap.min.css')}}\">

        <!-- Font Awesome -->
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/main/font-awesome/css/font-awesome.min.css')}}\">

        <!-- Ionicons -->
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/main/ionicons/ionicons.min.css')}}\">

        <!-- Theme style -->
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/main/dist/css/AdminLTE.min.css')}}\">

        <!-- iCheck -->
        <link rel=\"stylesheet\" href=\"{{ asset('bundles/main/plugins/iCheck/square/blue.css')}}\">
    {% endblock %}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src=\"{ { asset('bundles/main/html5shiv/html5shiv.min.js')}}\"></script>-->
    <!--<script src=\"{ { asset('bundles/main/respond/respond.min.js')}}\"></script>-->
    <![endif]-->
</head>
<body class=\"hold-transition login-page\">

{% block contenido %}

{% endblock %}

{% block javascripts %}
    <!-- jQuery 2.1.4 -->
    <script src=\"{{ asset('bundles/main/plugins/jQuery/jQuery-2.1.4.min.js')}}\"></script>

    <!-- Bootstrap 3.3.5 -->
    <script src=\"{{ asset('bundles/main/bootstrap/js/bootstrap.min.js')}}\"></script>

    <!-- iCheck -->
    <script src=\"{{ asset('bundles/main/plugins/iCheck/icheck.min.js')}}\"></script>

    <script>
        \$(function () {
            \$('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
{% endblock %}

</body>
</html>
", "MainBundle::template.html.twig", "/var/www/html/taxi-backend/src/MainBundle/Resources/views/template.html.twig");
    }
}
